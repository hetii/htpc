#include <espressif/esp_common.h>
#include <FreeRTOS.h>
#include <task.h>
#include "irmp/irsnd.h"
#include "irmp/irmp.h"
#include "irmpsnd.h"

static void task_print_ir_codes(void *pvParameters) {
  IRMP_DATA  irmp_data;
  for (;;){
    if (irmp_get_data(&irmp_data)){
      printf("protocol: 0x%x", irmp_data.protocol);

      #if IRMP_PROTOCOL_NAMES == 1
        printf("   %s", irmp_protocol_names[irmp_data.protocol]);
      #endif
      printf("   address: 0x%x", irmp_data.address);
      printf("   command: 0x%x", irmp_data.command);
      printf("   flags: 0x%x", irmp_data.flags);  
      printf("\r\n");
    }
  }
}

void irmpsnd_init(void){
  _xt_isr_attach(INUM_TIMER_FRC1, (_xt_isr)irmp_ISR);
  
  timer_set_frequency(FRC1, F_INTERRUPTS); // 15KHz ~66.6uS
  timer_set_interrupts(FRC1, true);
  timer_set_run(FRC1, true);
  
  irmp_init();
  xTaskCreate(task_print_ir_codes, "task_print_ir_codes", 256, NULL, 2, NULL);
}