#include "espressif/esp_common.h"
#include "FreeRTOS.h"
#include "spi.h"
// https://github.com/esp8266/Arduino/blob/master/libraries/SPISlave/src/hspi_slave.c
// https://github.com/esp8266/Arduino/blob/master/cores/esp8266/esp8266_peri.h
// https://github.com/boseji/ESP-Store/blob/master/manuals/Archive/8I-ESP8266__SPI-WiFi_Passthrough_1-Interrupt_Mode__EN_v0.1.pdf

//SPI0, SPI1 & I2S Interupt Register
#define ESP8266_DREG(addr) *((volatile uint32_t *)(0x3FF00000+(addr)))
#define SPIIR ESP8266_DREG(0x20)
#define SPII0 4 //SPI0 Interrupt
#define SPII1 7 //SPI1 Interrupt
#define SPII2 9 //I2S Interrupt

static void (*_hspi_slave_rx_data_cb)(uint8_t * data, uint8_t len) = NULL;
static void (*_hspi_slave_tx_data_cb)(void) = NULL;
static void (*_hspi_slave_rx_status_cb)(uint32_t data) = NULL;
static void (*_hspi_slave_tx_status_cb)(void) = NULL;
static uint8_t _hspi_slave_buffer[33]={0};

void hspi_slave_setData(uint8_t *data, uint8_t len);

static IRAM void spi_slave_isr_handler(void){

  uint32_t status;
  uint32_t istatus;

  istatus = SPIIR;
  // SPI1 ISR
  if(istatus & (1 << SPII1)) {
    status = SPI(1).SLAVE0;
    
    // Disable interrupts.
    SPI(1).SLAVE0 &= ~(SPI_SLAVE0_TRANS_DONE_EN  | \
                       SPI_SLAVE0_WR_STA_DONE_EN | \
                       SPI_SLAVE0_RD_STA_DONE_EN | \
                       SPI_SLAVE0_WR_BUF_DONE_EN | \
                       SPI_SLAVE0_RD_BUF_DONE_EN);
    // Reset.
    SPI(1).SLAVE0 |= SPI_SLAVE0_SYNC_RESET;

    // Clear interrupts.
    SPI(1).SLAVE0 &= ~(SPI_SLAVE0_TRANS_DONE  | \
                       SPI_SLAVE0_WR_STA_DONE | \
                       SPI_SLAVE0_RD_STA_DONE | \
                       SPI_SLAVE0_WR_BUF_DONE | \
                       SPI_SLAVE0_RD_BUF_DONE);

    // Enable interrupts.
    SPI(1).SLAVE0 |=  (SPI_SLAVE0_TRANS_DONE_EN  | \
                       SPI_SLAVE0_WR_STA_DONE_EN | \
                       SPI_SLAVE0_RD_STA_DONE_EN | \
                       SPI_SLAVE0_WR_BUF_DONE_EN | \
                       SPI_SLAVE0_RD_BUF_DONE_EN);

    if ((status & SPI_SLAVE0_RD_BUF_DONE) && _hspi_slave_tx_data_cb){
      printf("SPI_SLAVE0_RD_BUF_DONE\n");
      _hspi_slave_tx_data_cb();
    }

    if((status & SPI_SLAVE0_RD_STA_DONE) && _hspi_slave_tx_status_cb){
      printf("SPI_SLAVE0_RD_STA_DONE\n");
      _hspi_slave_tx_status_cb();
    }
    if((status & SPI_SLAVE0_WR_STA_DONE) && _hspi_slave_rx_status_cb){
      uint32_t s = SPI(1).WSTATUS;
      printf("SPI_SLAVE0_WR_STA_DONE 0x%.4X\n", s);
      SPI(1).RSTATUS = s;

      //SPI(1).WSTATUS = (uint32_t)0x1abd;
      _hspi_slave_rx_status_cb(s);
    }

    if((status & SPI_SLAVE0_WR_BUF_DONE) != 0 && (_hspi_slave_rx_data_cb)) {
        uint8_t i;
        uint32_t data;
        _hspi_slave_buffer[32] = 0;
       
        for(i=0; i<8; i++) {
          data = SPI(1).W[i];
          _hspi_slave_buffer[i<<2] = (data >> 24) & 0xff;
          _hspi_slave_buffer[(i<<2)+1] = (data >> 16) & 0xff;
          _hspi_slave_buffer[(i<<2)+2] = (data >> 8) & 0xff;
          _hspi_slave_buffer[(i<<2)+3] = (data & 0xff);
        }
        _hspi_slave_rx_data_cb(&_hspi_slave_buffer[0], 32);
    }
  // SPI0 ISR
  } else if(istatus & (1 << SPII0)) {
    printf("SPI0 ISR occure\n");
    SPI(0).SLAVE0 &= ~(0x3ff); // clear SPI ISR 
  // I2S ISR
  } else if(istatus & (1 << SPII2)) {
    printf("SPII2 ISR occure\n");
  } 
}

void spi_slave_init(uint8_t bus, void (*tx_data_cb)(void), void (*tx_status_cb)(void), void (*rx_status_cb)(uint32_t), void (*rx_data_cb)(uint8_t *, uint8_t)){
  //clear bit9,bit8 of reg PERIPHS_IO_MUX
  //bit9 should be cleared when HSPI clock doesn't equal CPU clock
  //bit8 should be cleared when SPI clock doesn't equal CPU clock
  ////WRITE_PERI_REG(PERIPHS_IO_MUX, 0x105); //clear bit9//TEST
  if(bus==0){
    gpio_set_iomux_function(6, IOMUX_FUNC(1));    // SPICLK - IOMUX_GPIO6_FUNC_SD_CLK
    gpio_set_iomux_function(7, IOMUX_FUNC(1));    // SPIQ   - IOMUX_GPIO7_FUNC_SD_DATA0
    gpio_set_iomux_function(8, IOMUX_FUNC(1));    // SPID   - IOMUX_GPIO8_FUNC_SD_DATA1 
    //gpio_set_iomux_function(11, IOMUX_FUNC(1)); // SPICS0 - IOMUX_GPIO11_FUNC_SD_CMD  

  // Configure MUX to allow HSPI
  }else if(bus==1){
    // WRITE_PERI_REG(PERIPHS_IO_MUX, 0x105);
    // SCK, MOSI, MISO and CS
    gpio_set_iomux_function(12, IOMUX_FUNC(2));   // IOMUX_GPIO12_FUNC_MTDI
    gpio_set_iomux_function(13, IOMUX_FUNC(2));   // IOMUX_GPIO13_FUNC_MTCK
    gpio_set_iomux_function(14, IOMUX_FUNC(2));   // IOMUX_GPIO14_FUNC_MTMS
    //gpio_set_iomux_function(15, IOMUX_FUNC(2)); // IOMUX_GPIO15_FUNC_MTDO
  }

  if (tx_data_cb) _hspi_slave_tx_data_cb = tx_data_cb;
  if (tx_status_cb) _hspi_slave_tx_status_cb = tx_status_cb;
  if (rx_status_cb) _hspi_slave_rx_status_cb = rx_status_cb;
  if (rx_data_cb) _hspi_slave_rx_data_cb = rx_data_cb;

  SPI(bus).SLAVE0 = SPI_SLAVE0_MODE | SPI_SLAVE0_WR_RD_BUF_EN | \
  SPI_SLAVE0_WR_BUF_DONE_EN | SPI_SLAVE0_RD_BUF_DONE_EN | \
  SPI_SLAVE0_WR_STA_DONE_EN | SPI_SLAVE0_RD_STA_DONE_EN | \
  SPI_SLAVE0_TRANS_DONE_EN;

  // MISO pahse uses W8-W15, SPI_USR_DIN_HIGHPART |
  // COMMAND pahse, SPI_USR_COMMAND
  // SPI Slave Edge (0:falling, 1:rising), SPI_CK_I_EDGE
  SPI(bus).USER0 = SPI_USER0_MISO_HIGHPART | SPI_USER0_COMMAND | SPI_USER0_CLOCK_IN_EDGE;
  SPI(bus).CLOCK = 0;

  // 4 bit in SPIxU2 default 7 (8bit)
  SPI(bus).USER2 = ((0x7 & SPI_USER2_COMMAND_BITLEN_M) << SPI_USER2_COMMAND_BITLEN_S);

  // 956243056 = 0x38ff1c70
  SPI(bus).SLAVE1 = 0x38ff1c70;

  // Set 8 bit slave recieve buffer length, the buffer is SPI_FLASH_C0-C7.
  // Set 8 bit slave status register, which is the low 8 bit of register "SPI_FLASH_STATUS".
/*
  SPI(bus).SLAVE1 |= ((0xff & SPI_SLAVE1_BUF_BITLEN_M) << SPI_SLAVE1_BUF_BITLEN_S) | \
  ((0x7&SPI_SLAVE1_STATUS_BITLEN_M)<<SPI_SLAVE1_STATUS_BITLEN_S) | \
  ((0x7&SPI_SLAVE1_WR_ADDR_BITLEN_M)<<SPI_SLAVE1_WR_ADDR_BITLEN_S) | \
  ((0x7&SPI_SLAVE1_RD_ADDR_BITLEN_M)<<SPI_SLAVE1_RD_ADDR_BITLEN_S);
  */

  SPI(bus).PIN = (1 << 19);  // What this is ??

  // Two lines below is to configure spi timing.
  // SPI(bus).CTRL2 |= ( (0x2&SPI_CTRL2_MOSI_DELAY_NUM_M) << SPI_CTRL2_MOSI_DELAY_NUM_S);
  
  SPI(bus).CMD = SPI_CMD_USR;

  // Register isr function, which contains spi, hspi and i2s events
  // Set SPI handler
  _xt_isr_attach(ETS_SPI_INUM, spi_slave_isr_handler);
  _xt_isr_unmask(1 << ETS_SPI_INUM);
  
uint8_t msg[32]={0};
  msg[0]=0x81;
  msg[1]=0x82;
  msg[2]=0x83;
  msg[3]=0x84;
  msg[4]=0x85;
  msg[5]=0x86;
  msg[6]=0x87;
  msg[7]=0x88;
  msg[31]=0xef;
  hspi_slave_setData(msg, 32);
}

#define ESP8266_REG(addr) *((volatile uint32_t *)(0x60000000+(addr)))
#define SPI1W(p) ESP8266_REG(0x140 + ((p & 0xF) * 4))

void hspi_slave_setData(uint8_t *data, uint8_t len){
    uint8_t i;
    uint32_t out = 0;
    uint8_t bi = 0;
    uint8_t wi = 8;

    for(i=0; i<32; i++) {
        out |= (i<len)?(data[i] << (bi * 8)):0;
        bi++;
        bi &= 3;
        if(!bi) {
            SPI1W(wi) = out;
            out = 0;
            wi++;
        }
    }
}