#ifndef _SLAVESPI_H_
#define _SLAVESPI_H_

/*------------------------------------------------------------------------------------------------------------------------------*
 |                         Data workflow bettwen master spi device and this hspi slave driver!                                  |
 |------------------------------------------------------------------------------------------------------------------------------|
 |  Command name | Bit set in status registry | First request                        | Second request                           | 
 |------------------------------------------------------------------------------------------------------------------------------|
 |  write status |   SPI_SLAVE0_WR_STA_DONE   | 0x01 + status (2bytes, 8 bits/word)  | None                                     |
 |------------------------------------------------------------------------------------------------------------------------------|
 |  write data   |   SPI_SLAVE0_WR_BUF_DONE   | 0x02 + 0x00   (2bytes, 8 bits/word)  | 32 bytes of data (32bytes, 32 bits/word) |
 |------------------------------------------------------------------------------------------------------------------------------|
 |  read data    |   SPI_SLAVE0_RD_BUF_DONE   | 0x03 + 0x00   (2bytes, 8 bits/word)  | 32 bytes of data (32bytes, 8 bits/word)  |
 |------------------------------------------------------------------------------------------------------------------------------|
 |  read status  |   SPI_SLAVE0_RD_STA_DONE   | 0x04 + 0x00   (2bytes, 8 bits/word)  | None                                     | 
 *------------------------------------------------------------------------------------------------------------------------------*/

void spi_slave_init(uint8_t bus, void (*tx_data_cb)(void), void (*tx_status_cb)(void), void (*rx_status_cb)(uint32_t), void (*rx_data_cb)(uint8_t *, uint8_t));

#endif