#ifndef _SERVERS_H_
#define _SERVERS_H_
#include <lwip/api.h>
#include <stdint.h> 

#define streq(str1, str2) (strncmp(str1, str2, sizeof(str2)-1) == 0)

typedef struct {
	uint16_t port;
	void (*on_connect)(struct netconn *conn);
	void (*on_close)(struct netconn *conn);
	void (*on_receive)(struct netconn *conn, char *data, uint32_t len);

} tcp_server_config_t;

void init_tcp_server(tcp_server_config_t *conf);
void telnet_commands(struct netconn *conn, char *data, uint32_t len);

#endif