#include <ipv4/lwip/ip_addr.h>
#include <lwip/api.h>
#include <espressif/esp_system.h>
#include <espressif/user_interface.h>
#include <espressif/esp_misc.h>
#include <string.h>
#include "servers.h"

void task_tcp_server(void *pvParameters){
  printf("START: task_tcp_server\n");
  struct netconn *conn, *newconn;
  err_t err;
  tcp_server_config_t *tcp_srv_cfg = (tcp_server_config_t *)pvParameters;
 
  // Create a new connection identifier.
  conn = netconn_new(NETCONN_TCP);
  
  if (conn!=NULL){
    // Bind connection to well known port number 7.
    printf("TCP server will listen on %d\n", tcp_srv_cfg->port);
    err = netconn_bind(conn, NULL, tcp_srv_cfg->port);
    if (err == ERR_OK){
      // Tell connection to go into listening mode.
      netconn_listen(conn);
      while (1){
        // Grab new connection.
        err = netconn_accept(conn, &newconn);
        
        // Process the new connection.
        if(err == ERR_OK){
          struct netbuf *buf;
          void *data;
          u16_t len;
          if (tcp_srv_cfg->on_connect != NULL) tcp_srv_cfg->on_connect(newconn);
          //netconn_set_recvtimeout(newconn, 2000);
          while((err = netconn_recv(newconn, &buf)) == ERR_OK){
            do{
              netbuf_data(buf, &data, &len);
              if (tcp_srv_cfg->on_receive != NULL) tcp_srv_cfg->on_receive(newconn, data, len);
            } while (netbuf_next(buf) >= 0);
            netbuf_delete(buf);
          }
          // Close connection and discard connection identifier.
          netconn_close(newconn);
          if (tcp_srv_cfg->on_close != NULL) tcp_srv_cfg->on_close(newconn);
          netconn_delete(newconn);
        }
      }
    } else {
      printf("Can not bind TCP netconn.\n");
    }
  } else {
    printf("Can not create TCP netconn.\n");
  }
}

void init_tcp_server(tcp_server_config_t *conf){
  xTaskCreate(task_tcp_server, "task_tcp_server", 512, conf, 2, NULL);
}

void telnet_commands(struct netconn *conn, char *data, uint32_t len){
  char msg[255]={0};
  uint8_t msg_len = 0;
  
  if(streq(data, "ping")){
    netconn_write(conn, "pong\n", 5, NETCONN_COPY);
  } else if (streq(data, "ifconfig")){
    uint8_t mac[6];
    struct ip_info ip;
    
    sdk_wifi_get_ip_info(STATION_IF, &ip);
    sdk_wifi_get_macaddr(STATION_IF, mac);
    
    msg_len = snprintf(msg, 255, "IP: %d.%d.%d.%d\n" \
                                 "GW: %d.%d.%d.%d\n" \
                                 "NETMASK: %d.%d.%d.%d\n" \
                                 "MAC: %02X:%02X:%02X:%02X:%02X:%02X\n", \
    IP2STR(&ip.ip), IP2STR(&ip.gw), IP2STR(&ip.netmask), mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  } else if (streq(data, "iwconfig")){
    //int32_t rssi = sdk_wifi_station_get_rssi();

  } else if (streq(data, "mem")){
    uint32_t free = sdk_system_get_free_heap_size();
    len = snprintf(msg, 100, "Free heap is: %d\n\n", free);
    sdk_system_print_meminfo();
  } else if (streq(data, "dhcpc stop")){
    sdk_wifi_station_dhcpc_stop();
  } else if (streq(data, "dhcpc start")){
    sdk_wifi_station_dhcpc_start();
  } else if (streq(data, "dhcpc restart")){
    sdk_wifi_station_dhcpc_stop();
    sdk_wifi_station_dhcpc_start();
  }

  if (msg_len>0){
    netconn_write(conn, msg, msg_len, NETCONN_COPY);
  }
}

