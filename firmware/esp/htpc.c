#include <esp/uart.h>
#include <string.h>
#include <lwip/netif.h>
#include <lwip/api.h>

#include "espressif/esp_common.h"
#include "FreeRTOS.h"
#include "task.h"
#include "ssid_config.h"
#include "ota-tftp.h"
#include "servers.h"
#include "irmpsnd.h"
#include "pixel.h"
#include "spi.h"

#define WS_POWER_STATUS   0x01
#define OD_POWER_STATUS   0x02
#define PS_POWER_STATUS   0x04

#define GPIO_PWR_IR_LED 2
#define GPIO_PWR_PS 16
#define GPIO_PWR_WS 4
#define GPIO_PWR_OD 5

static void ps_on(void);
static void ps_off(void);
static void ws_on(void);
static void ws_off(void);
static void od_on(void);
static void od_off(void);

static uint8_t status = 0;

static void ps_on(void){
  if (!(status & PS_POWER_STATUS)){
    gpio_write(GPIO_PWR_PS, 1);
    // We need to wait a while to GPIO_IS_HIGH +5v
    // on power_good line.
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    status |= PS_POWER_STATUS;
  }
}

static void ps_off(void){
  if ((status & PS_POWER_STATUS)){
    gpio_write(GPIO_PWR_PS, 0);
    status &= ~PS_POWER_STATUS;
    ws_off();
    od_off();
  }
}

static void ws_on(void){
  if (!(status & WS_POWER_STATUS)){
    ps_on();
    gpio_write(GPIO_PWR_WS, 0);
    status |= WS_POWER_STATUS;
  }
}

static void ws_off(void){
  if ((status & WS_POWER_STATUS)){
    gpio_write(GPIO_PWR_WS, 1);
    status &= ~WS_POWER_STATUS;
  }
}

static void od_on(void){
  if (!(status & OD_POWER_STATUS)){
    ps_on();

    gpio_enable(GPIO_PWR_OD, GPIO_OUTPUT);
    gpio_write(GPIO_PWR_OD, 0); // OD on

    vTaskDelay(1000 / portTICK_PERIOD_MS);
    gpio_enable(GPIO_PWR_OD, GPIO_INPUT);
    // Current should go throught 10k so I disable internnal pullup.
    gpio_set_pullup(GPIO_PWR_OD, false, false);
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    if (gpio_read(GPIO_PWR_OD) != 0){
      status |= OD_POWER_STATUS;
      printf("Odroid is on ;)\n");
    } else {
      printf("Odroid don`t start, try again ;-(\n");
      vTaskDelay(2000 / portTICK_PERIOD_MS);
      od_on();
    }
  }
}

static void od_off(void){
  if ((status & OD_POWER_STATUS)){
    status &= ~OD_POWER_STATUS;
    // Just enable internall pullup when external one is not self-powered by mosfet.
    // Not really needed but I prefer to keep it and don`t have floating gate of Q4.
    gpio_set_pullup(GPIO_PWR_OD, true, true);
    // Alternative you can drive this pin high but then more current will go throught R37.
    // gpio_enable(GPIO_PWR_OD, GPIO_OUTPUT);
    // gpio_write(GPIO_PWR_OD, 1);
  }
}

void task_power_control(void *pvParameters){
  static uint8_t old_status=0;

  while(1) {
    if (gpio_read(GPIO_PWR_OD) == 0){
      od_off();
    }

    if (old_status != status){
      old_status = status;
      printf("Current power status is:\n");
      (status & PS_POWER_STATUS) ? printf("PS is ON\n") : printf("PS is OFF\n");
      (status & WS_POWER_STATUS) ? printf("WS is ON\n") : printf("WS is OFF\n");
      (status & OD_POWER_STATUS) ? printf("OD is ON\n") : printf("OD is OFF\n");

      if (((status & PS_POWER_STATUS)) && (!(status & WS_POWER_STATUS)) && (!(status & OD_POWER_STATUS))){
        vTaskDelay(10000 / portTICK_PERIOD_MS);
        if ((!(status & WS_POWER_STATUS)) && (!(status & OD_POWER_STATUS))){
          ps_off();
        }
      }
    }
    vTaskDelay(2000 / portTICK_PERIOD_MS);
  }
}

void on_telnet_receive(struct netconn *conn, char *data, uint32_t len){
  char msg[100]={0};
  telnet_commands(conn, data, len);

  if (streq(data, "ps on")){
    ps_on();
  } else if (streq(data, "ps off")){
    ps_off();
  } else if (streq(data, "od on")){
    od_on();
  } else if (streq(data, "od off")){
    od_off();
  } else if (streq(data, "ws on")){
    ws_on();
  } else if (streq(data, "ws off")){
    ws_off();
  } else if (streq(data, "status")){
    int len = snprintf(msg, 100, "odroid power line status is: %d\n\n", gpio_read(GPIO_PWR_OD));
    netconn_write(conn, msg, len, NETCONN_COPY);
  } else if (streq(data, "avr ")){
    printf("%s", data);
  }
}

void on_telnet_close(struct netconn *conn){
  netconn_write(conn, "Good bye ;-(", 12, NETCONN_COPY);
  printf("Connection lost!\n");
}

void on_telnet_connect(struct netconn *conn){
  netconn_write(conn, "Welcome in HTPC console.\n", 25, NETCONN_COPY);
}

void task_spi(void *pvParameters){
  printf("task_spi executed!\n");

  while(1){
    vTaskDelay(2000 / portTICK_PERIOD_MS);
  }
}

void on_hspi_data(uint8_t *data, uint8_t len){

  printf("HSPI got data:\n");
  uint32_t ret = 0;

  for (ret = 0; ret < len; ret++) {
    if (!(ret % 8)) printf(" ");
    printf("%.2X ", data[ret]);
  }
  printf("\n");
}

void on_hspi_tx_data(void){
  printf("on_hspi_tx_data\n");
}

void on_hspi_tx_status_cb(void){
  printf("on_hspi_tx_status_cb\n");
}

void on_hspi_rx_status_cb(uint32_t status){
  printf("on_hspi_rx_status_cb\n");
}

void user_init(void){
  static tcp_server_config_t telnet_cfg;
  
  telnet_cfg.port = 23;
  telnet_cfg.on_connect = on_telnet_connect;
  telnet_cfg.on_receive = on_telnet_receive;
  telnet_cfg.on_close = on_telnet_close;

  struct sdk_station_config config = {
      .ssid = WIFI_SSID,
      .password = WIFI_PASS,
  };

  uart_set_baud(0, 115200);
  printf("Odroid HTPC based use SDK version:%s\n", sdk_system_get_sdk_version());

  gpio_enable(GPIO_PWR_PS, GPIO_OUTPUT);
  gpio_enable(GPIO_PWR_WS, GPIO_OUTPUT);
  gpio_enable(GPIO_PWR_OD, GPIO_OUTPUT);
  gpio_enable(GPIO_PWR_IR_LED, GPIO_OUTPUT);
  gpio_write(GPIO_PWR_IR_LED, 0);

  ps_on();
  ws_off();
  od_on();
  
  // Required to call wifi_set_opmode before station_set_config.
  sdk_wifi_set_opmode(STATION_MODE);
  sdk_wifi_station_set_config(&config);

  // Change hostname.
  sdk_wifi_station_disconnect();
  netif_set_hostname(netif_default, "htpc");
  sdk_wifi_station_connect();

  // External task list.
  ota_tftp_init_server(TFTP_PORT);
  init_tcp_server(&telnet_cfg);

  // Internal task list.
  xTaskCreate(task_power_control, "task_power_control", 256, NULL, 2, NULL);
  xTaskCreate(&demo1, "ws2812_i2s", 512, NULL, 10, NULL);
  irmpsnd_init();

 // spi_slave_init(1, &on_hspi_tx_data,&on_hspi_tx_status_cb,&on_hspi_rx_status_cb, &on_hspi_data);
  //xTaskCreate(task_spi, "task_spi", 256, NULL, 2, NULL);
}
